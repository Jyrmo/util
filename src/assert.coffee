import { isString } from 'lodash'

import { AssertionFailedError } from './errors'

defaultErrorMsg = 'Assertion failed'

getDefaultError = (msg = defaultErrorMsg) -> new AssertionFailedError msg

export default (predicate, msgOrError) ->
  if not predicate
    error =
      if isString msgOrError then getDefaultError msgOrError else msgOrError
    throw error
