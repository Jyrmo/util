import { isInteger } from 'lodash'

import assert from '../assert'

assertInt = (val) ->
  msg = 'The given value is not an integer.'
  assert (isInteger val), msg

assertPositive = (val) ->
  msg = 'The given value is not positive.'
  assert (val > 0), msg

assertValidLineLength = (lineLength) ->
  assertInt lineLength
  assertPositive lineLength

wordWrap = (text, lineLength) ->
  # Assume text is string and lineLength is number of characters per line (int).
  # Returns array of strings of maximum length lineLength characters.
  # Attempts to break text at spaces if possible. Otherwise breaks when
  # lineLength characters reached.
  assertValidLineLength lineLength
  tail = text
  finished = false
  while not finished
    if tail.length <= lineLength
      finished = true
      tail
    else
      idx = tail.lastIndexOf ' ', lineLength
      if idx > -1
        line = tail[0...idx]
        tail = tail[idx + 1..]
        line
      else
        line = tail[0...lineLength]
        tail = tail[lineLength..]
        line

export { wordWrap }
