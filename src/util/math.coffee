isBetweenInclusive = (val, border1, border2) ->
  [lowBorder, highBorder] =
    if border1 > border2 then [border2, border1] else [border1, border2]
  lowBorder <= val <= highBorder

makeUpVector2 = ->
  {
    x: 0
    y: -1
  }

makeDownVector2 = ->
  {
    x: 0
    y: 1
  }

makeLeftVector2 = ->
  {
    x: -1
    y: 0
  }

makeRightVector2 = ->
  {
    x: 1
    y: 0
  }

addVector2 = (vector1, vector2) ->
  # Assume vector1 and vector2 are {x, y}
  {
    x: vector1.x + vector2.x
    y: vector1.y + vector2.y
  }

subtractVector2 = (vector1, vector2) ->
  {
    x: vector1.x - vector2.x
    y: vector1.y - vector2.y
  }

inverseVector2 = (vector) -> scalarMultiplyVector2(-1, vector)

reverseVector2 = inverseVector2

scalarMultiplyVector2 = (scalar, vector) ->
  {
    x: scalar * vector.x
    y: scalar * vector.y
  }

clamp = (val, startVal, endVal) ->
  if isBetweenInclusive val, startVal, endVal
    val
  else
    [lowBorder, highBorder] =
      if startVal > endVal then [endVal, startVal] else [startVal, endVal]
    if val < lowBorder then lowBorder
    else if val > highBorder then highBorder

lerp = (startVal, endVal, fraction) ->
  dist = endVal - startVal
  distFraction = fraction * dist
  startVal + distFraction

clampedLerp = (startVal, endVal, fraction) ->
  clampedFraction = clamp fraction, 0, 1
  lerp startVal, endVal, clampedFraction

lerpVector2 = (startVector, endVector, fraction) ->
  {
    x: lerp startVector.x, endVector.x, fraction
    y: lerp startVector.y, endVector.y, fraction
  }

clampedLerpVector2 = (startVector, endVector, fraction) ->
  clampedFraction = clamp fraction, 0, 1
  lerpVector2 startVector, endVector, clampedFraction

export { isBetweenInclusive,
  makeUpVector2, makeDownVector2, makeLeftVector2, makeRightVector2,
  addVector2, subtractVector2,
  inverseVector2, scalarMultiplyVector2,
  clamp, lerp, clampedLerp, lerpVector2, clampedLerpVector2 }
