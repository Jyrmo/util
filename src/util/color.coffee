decToZeroPaddedTwoDigitHex = (dec) ->
  strHex = dec.toString 16
  if strHex.length < 2 then "0#{strHex}" else strHex

createRgbaColor = (r, g, b, a = 255) -> { r, g, b, a }

createRgbColor = (r, g, b) -> { r, g, b }

rgbaColorToRgbColor = (rgba) ->
  {
    r: rgba.r
    g: rgba.g
    b: rgba.b
  }

rgbaColorToHexStr = (rgba) ->
  # Assuming rgba is {r, g, b, a} and rgba are integers in range 0-255,
  # returns hex string formatted "#rrggbbaa"
  hexStrR = decToZeroPaddedTwoDigitHex rgba.r
  hexStrG = decToZeroPaddedTwoDigitHex rgba.g
  hexStrB = decToZeroPaddedTwoDigitHex rgba.b
  hexStrA = decToZeroPaddedTwoDigitHex rgba.a
  "\##{hexStrR}#{hexStrG}#{hexStrB}#{hexStrA}"

rgbColorToHexStr = (rgb) ->
  # Assuming rgb is {r, g, b} and rgb are integers in range 0-255,
  # returns hex string formatted "#rrggbb"
  hexStrR = decToZeroPaddedTwoDigitHex rgb.r
  hexStrG = decToZeroPaddedTwoDigitHex rgb.g
  hexStrB = decToZeroPaddedTwoDigitHex rgb.b
  "\##{hexStrR}#{hexStrG}#{hexStrB}"

export { createRgbaColor, createRgbColor, rgbaColorToRgbColor,
  rgbaColorToHexStr, rgbColorToHexStr }
