import { isBetweenInclusive } from './math'

{ sqrt, pow } = Math

getDistance = (point1, point2) ->
  xDist = point1.x - point2.x
  yDist = point1.y - point2.y
  sqrt ((pow xDist, 2) + (pow yDist, 2))

isPointInRect = (point, rect) ->
  # Assume rect = {start, end} and startPoint, endPoint are
  # points {x, y}
  isXBetween = isBetweenInclusive point.x, rect.start.x, rect.end.x
  isYBetween = isBetweenInclusive point.y, rect.start.y, rect.end.y
  isXBetween and isYBetween

isPointInCircle = (point, circle) ->
  # Assume circle = {center, radius} and center is point {x, y}
  distPointCenter = getDistance point, circle.center
  distPointCenter <= circle.radius

export { getDistance, isPointInRect, isPointInCircle }
